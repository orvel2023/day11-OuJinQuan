import ErrorPage from '../pages/ErrorPage'
import HelpPage from '../pages/HelpPage'
import App from '../App'
import DoneListGroup from '../components/DoneListGroup'
import TodoList from '../components/TodoList'

const config = [{
  path: '/',
  element: <App />,
  children: [
    {
      path: '/',
      element: <TodoList />,
    },
    {
      path: '/home',
      element: <TodoList />,
    },
    {
      path: '/donelist',
      element: <DoneListGroup />,
    },
    {
      path: '/help',
      element: <HelpPage />,
    },
  ],
  errorElement: <ErrorPage></ErrorPage>
}
]
export default config;