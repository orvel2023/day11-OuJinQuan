import React, { useState } from 'react'
import { useReloadTodoListData } from '../hook/reloadTodoListData'
import { Input } from 'antd'
const { Search } = Input

export default function TodoGenerator() {
    const { addTodo } = useReloadTodoListData()
    const [inputValue, setInputValue] = useState("")
    const appendTodoListItem = () => {
        if (inputValue === "") { return }
        addTodo({
            content: inputValue,
            done: false
        })
        setInputValue("")
    }
    const changeInputValue = (event) => {
        setInputValue(event.target.value)
    }
    return (
        <div className="generator-box">
            <Search
                id="inputContent"
                allowClear
                size="large"
                enterButton="Add"
                placeholder="input list item content"
                onSearch={appendTodoListItem}
                onChange={changeInputValue}
                value={inputValue}
                />

        </div>
    )
}