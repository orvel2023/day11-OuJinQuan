import React from 'react'
import TodoGenerator from './TodoGenerator'
import TodoListGroup from './TodoListGroup'
export default function TodoList() {

    return (
        <div>
            <h1>Todo List</h1>
            <TodoGenerator></TodoGenerator>
            <TodoListGroup></TodoListGroup>
        </div>

    )
}
