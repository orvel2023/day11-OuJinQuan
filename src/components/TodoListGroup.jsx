import React, { useState } from 'react'
import { DeleteTwoTone, EditTwoTone, EyeTwoTone } from '@ant-design/icons'
import { Modal, Input, Popconfirm, List } from 'antd'
import { useReloadTodoListData } from '../hook/reloadTodoListData'
import { useSelector } from 'react-redux'

const { TextArea } = Input;
export default function TodoListItem() {
    const { deleteTodo, editTodo, getTodo } = useReloadTodoListData()
    const todoList = useSelector((store) => {
        return store.todoList.value
    })
    const [editItemValue, setEditItemValue] = useState({})
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [isDetailModalOpen, setIsDetailModalOpen] = useState(false)
    const [editValue, seteditValue] = useState("")
    const [itemDetail, setItemDetail] = useState({})
    const deleteListItem = (itemObj) => {
        deleteTodo(itemObj.id)
    }
    const editListItem = (itemObj) => {
        setEditItemValue(itemObj)
        seteditValue(itemObj.content)
        setIsModalOpen(true)
    }

    const editValueChange = (event) => {
        seteditValue(event.target.value)
    }
    const getTodoItemDetail = (id) =>{
        getTodo(id).then((response) => {
            setItemDetail(response.data)
            setIsDetailModalOpen(true)
        })
    } 
    const handleOk = () => {
        editTodo({
            id: editItemValue.id,
            content: editValue,
            done: editItemValue.done
        })
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };
    const toggleItemState = (item) => {
        editTodo({
            id: item.id,
            content: item.content,
            done: !item.done
        })
    }
    return (

        <div >
            <List
                pagination={{
                    position: 'bottom',
                    align: 'center'
                }}
                size="small"
                bordered
                dataSource={todoList}
                renderItem={(item) =>
                    <List.Item
                        style={{ cursor: 'pointer' }}
                        className={item.done ? "complete-item todolist-item" : 'todolist-item'}
                        actions={[<EyeTwoTone key="list-loadmore-detail" onClick={(e) => { getTodoItemDetail(item.id) }} />, <EditTwoTone key="list-loadmore-edit" onClick={(e) => { editListItem(item) }} />,
                        <Popconfirm
                            key="list-loadmore-delete"
                            title="Delete the task"
                            description="Are you sure to delete this task?"
                            onConfirm={(e) => { deleteListItem(item) }}
                            onCancel={() => { }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <DeleteTwoTone twoToneColor="#ff1030" />
                        </Popconfirm>]}
                    >
                        <span onClick={(e) => { toggleItemState(item) }} >{item.content}</span>
                        <div>
                        </div>
                    </List.Item>}
            />
            <Modal title="List Item Detail" open={isDetailModalOpen} onCancel={() => {setIsDetailModalOpen(false)}} footer={[]} >
                <p>id : {itemDetail.id}</p>
                <p>content : {itemDetail.content}</p>
                <p>done : {itemDetail.done ? "yes" : "no"}</p>
            </Modal>
            <Modal title="Edit List Item" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <TextArea row="3" value={editValue} onChange={editValueChange} ></TextArea>
            </Modal>
        </div>

    )
}