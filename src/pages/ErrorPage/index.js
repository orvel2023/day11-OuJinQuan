import { useRouteError } from 'react-router'
import './index.css'
export default function ErrorPage() {
  const error = useRouteError()
  return (
    <div className="text-wrap">
      <h1>{error.status}</h1>
      <div className="text-content">{error.statusText},页面不存在或者您没有权限访问。</div>
    </div>
  )
}