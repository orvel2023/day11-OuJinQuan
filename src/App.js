import './App.css'
import { Link, Outlet } from 'react-router-dom'
import { useReloadTodoListData } from './hook/reloadTodoListData'
import { Menu } from 'antd'
import { HomeOutlined, CheckSquareOutlined, QuestionCircleOutlined } from '@ant-design/icons'
import { useState, useEffect } from 'react'
const items = [
  {
    label: <Link to='/home'>Home</Link>,
    key: 'home',
    icon: <HomeOutlined />,
  },
  {
    label: <Link to='/donelist'>Done List</Link>,
    key: 'doneList',
    icon: <CheckSquareOutlined />,
  }, {
    label: <Link to='/help'>Help</Link>,
    key: 'help',
    icon: <QuestionCircleOutlined />
  },
]
function App() {
  const { reloadTodoListData } = useReloadTodoListData()
  useEffect(() => {
    reloadTodoListData()
  })
  const [current, setCurrent] = useState('home');
  const onClick = (e) => {
    setCurrent(e.key)
  }
  return (
    <div className="App">
      <nav>
        <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />
      </nav>
      <div>
        <Outlet></Outlet>
      </div>
    </div>
  );
}

export default App;
