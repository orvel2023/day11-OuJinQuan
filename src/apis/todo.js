import api from './api'

export const getTodoTasks = () => {
    return api.get('/todolist')
}

export const getTodoTaskItem = (id) => {
    return api.get(`/todolist/${id}`)
}

export const deleteTodoTaskItem = (id) => {
    return api.delete(`/todolist/${id}`)
}

export const addTodoTaskItem = (itemObj) => {
    return api.post('/todolist', itemObj)
}

export const updateTodoTaskItem = (itemObj) => {
    return api.put(`/todolist/${itemObj.id}`, itemObj)
}