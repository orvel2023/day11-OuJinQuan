import { configureStore } from '@reduxjs/toolkit'
import todoListSlice from './reducer/todoList'

const store = configureStore({
    reducer: { todoList: todoListSlice.reducer }
});
export default store
