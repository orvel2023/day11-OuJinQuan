import { createSlice } from '@reduxjs/toolkit'
const todoListSlice = createSlice({
  name: 'todoList',
  initialState: {
    value: []
  },
  reducers: {
    initItem: (state, actions) => {
      state.value = actions.payload
    },
    addItem: (state, actions) => {
      state.value.push(actions.payload)
    }

  }
})
export const { initItem, addItem } = todoListSlice.actions;
export default todoListSlice;