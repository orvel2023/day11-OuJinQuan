import { getTodoTasks, deleteTodoTaskItem, updateTodoTaskItem, addTodoTaskItem, getTodoTaskItem } from '../apis/todo'
import { useDispatch } from 'react-redux'
import { initItem } from '../redux/reducer/todoList'
import { message } from 'antd'

export function useReloadTodoListData() {
  const dispatch = useDispatch()
  async function reloadTodoListData() {
    const responce = await getTodoTasks()
    dispatch(initItem(responce.data))

  }
  const getTodo = async (id) => {
    return  await getTodoTaskItem(id)
  }
  const deleteTodo = async (id) => {
    await deleteTodoTaskItem(id)
    await reloadTodoListData()
    message.success('delete success')
  }
  const editTodo = async (itemObj) => {
    await updateTodoTaskItem(itemObj)
    await reloadTodoListData()
  }
  const addTodo = async (itemObj) => {
    await addTodoTaskItem(itemObj)
    await reloadTodoListData()
    message.success('New success!')
  }

  return { reloadTodoListData, deleteTodo, editTodo, addTodo, getTodo }
}
