# O
  - Coordinate the front and back ends : I learned how to do a project that separated the front and back ends and let them communicate, which was closer to the real project scene and laid the foundation for next week's simulation project.
  - Cross-domain issues: Cross-domain is when the browser cannot execute scripts for other websites. It is caused by the same-origin policy of the browser and is a security restriction imposed by the browser. There are many ways to solve the cross-domain, basically through the server Settings to allow cross-domain, front-end Settings proxy server, etc., today the use of Java cors configuration to allow cross-domain, learn how to write some detailed configuration.
  - Concepts related to Agile development: User story, user journey, elevator speech, MVP products and other concepts have made me understand the whole development process, and have a clearer understanding of the whole development process.


# R
  -  Helpful!
# I
  - I think today's let me have a deeper understanding of front-end and back-end communication. Gave me a deeper understanding of front-end knowledge system.
# D
  - Learn more about how to agile development.
  - Learn more about the responsibilities of various roles in Agile development.
